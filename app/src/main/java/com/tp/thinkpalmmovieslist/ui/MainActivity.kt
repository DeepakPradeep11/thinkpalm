package com.tp.thinkpalmmovieslist.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tp.thinkpalmmovieslist.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}