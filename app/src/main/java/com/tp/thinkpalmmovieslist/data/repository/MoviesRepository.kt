package com.tp.thinkpalmmovieslist.data.repository

import com.tp.thinkpalmmovieslist.data.models.PageResponseModel


/**
 * Created by Deepak Pradeep on 9/10/20.
 */
interface MoviesRepository {
    suspend fun getMoviePageContent(pageNumber: Int): PageResponseModel?
}