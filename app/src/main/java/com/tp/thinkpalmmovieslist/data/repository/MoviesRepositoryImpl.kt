package com.tp.thinkpalmmovieslist.data.repository

import android.content.Context
import com.google.gson.Gson
import com.tp.thinkpalmmovieslist.data.models.PageResponseModel
import com.tp.thinkpalmmovieslist.utility.AssetLoader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Created by Deepak Pradeep on 9/10/20.
 */
class MoviesRepositoryImpl(private val context: Context):MoviesRepository {
    override suspend fun getMoviePageContent(pageNumber: Int): PageResponseModel? {
        return withContext(Dispatchers.IO) {
            val jsonString = AssetLoader.getJsonFromAssets(context, getFileName(pageNumber))
            Gson().fromJson(jsonString, PageResponseModel::class.java)
        }
    }
    private fun getFileName(pageNumber: Int): String {
        return when (pageNumber) {
            1 -> "page1.json"
            2 -> "page2.json"
            3 -> "page3.json"
            else -> "page1.json"
        }
    }
}