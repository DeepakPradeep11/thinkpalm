package com.tp.thinkpalmmovieslist.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Deepak Pradeep on 9/10/20.
 */
class PageResponseModel : Serializable {
    @SerializedName("page")
    @Expose
    var page: Page? = null
}