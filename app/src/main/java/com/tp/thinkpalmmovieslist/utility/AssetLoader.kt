package com.tp.thinkpalmmovieslist.utility

import android.content.Context
import com.tp.thinkpalmmovieslist.R
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

/**
 * Created by Deepak Pradeep on 9/10/20.
 */
class AssetLoader {
    companion object {
        fun getJsonFromAssets(context: Context, fileName: String): String? {
            val jsonString: String
            jsonString = try {
                val inputStream: InputStream = context.assets.open(fileName)
                val size: Int = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                String(buffer, Charset.forName("UTF-8"))
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return jsonString
        }

        fun getImageForPoster(posterImage: String?): Int {
            return when(posterImage) {
                "poster1.jpg" -> R.drawable.poster1
                "poster2.jpg" -> R.drawable.poster2
                "poster3.jpg" -> R.drawable.poster3
                "poster4.jpg" -> R.drawable.poster4
                "poster5.jpg" -> R.drawable.poster5
                "poster6.jpg" -> R.drawable.poster6
                "poster7.jpg" -> R.drawable.poster7
                "poster8.jpg" -> R.drawable.poster8
                "poster9.jpg" -> R.drawable.poster9
                else -> R.drawable.placeholder_for_missing_posters
            }
        }
    }

}